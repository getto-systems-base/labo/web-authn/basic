package login

import (
	"github.com/getto-systems/webauthn-example/models"
)

type Input struct {
	Email string `json:"email"`
}

type DataStore interface {
	LookupUserByInput(Input) (*models.User, error)
	LookupUser(models.UserID) (*models.User, error)
}
