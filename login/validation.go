package login

import (
	"errors"

	"github.com/duo-labs/webauthn/webauthn"
)

var errCredentialCloned = errors.New("credential is cloned")

func Validate(credential webauthn.Credential) error {
	if credential.Authenticator.CloneWarning {
		return errCredentialCloned
	}

	return nil
}
