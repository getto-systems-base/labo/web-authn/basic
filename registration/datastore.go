package registration

import (
	"github.com/duo-labs/webauthn/webauthn"

	"github.com/getto-systems/webauthn-example/models"
)

type Input struct {
	Email string `json:"email"`
}

type DataStore interface {
	CheckDuplication(Input) error
	GenerateUserID() (models.UserID, error)
	RegisterUser(models.User) error
	LookupUser(models.UserID) (*models.User, error)
	RegisterUserCredential(models.User, webauthn.Credential) error
}
