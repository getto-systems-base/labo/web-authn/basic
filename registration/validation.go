package registration

import (
	"errors"
)

var errEmptyEmail = errors.New("email is empty")

func Validate(input Input) error {
	if len(input.Email) == 0 {
		return errEmptyEmail
	}

	return nil
}
