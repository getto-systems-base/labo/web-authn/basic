package models

import (
	"github.com/duo-labs/webauthn/webauthn"
)

type User struct {
	userID      UserID
	email       string
	credentials []webauthn.Credential
}

type UserID uint64

func NewUser(userID UserID, email string, credentials []webauthn.Credential) *User {
	return &User{userID, email, credentials}
}

func (user User) UserID() UserID {
	return user.userID
}
func (user User) Email() string {
	return user.email
}
func (user User) Credentials() []webauthn.Credential {
	return user.credentials
}
