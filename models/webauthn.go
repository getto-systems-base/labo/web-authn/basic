package models

import (
	"crypto/md5"
	"encoding/binary"
	"errors"
	"fmt"
	"strings"

	"github.com/duo-labs/webauthn/webauthn"
)

var errReadUserID = errors.New("failed to read User ID")

type webAuthnUser struct {
	*User
}

func (user webAuthnUser) WebAuthnID() []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	binary.PutUvarint(buf, uint64(user.UserID()))
	return buf
}

func (user webAuthnUser) WebAuthnName() string {
	email := user.Email()

	parts := strings.Split(email, "@")
	return parts[0]
}

func (user webAuthnUser) WebAuthnDisplayName() string {
	email := user.Email()
	return email
}

func (user webAuthnUser) WebAuthnIcon() string {
	data := []byte(user.Email())
	hash := fmt.Sprintf("%x", md5.Sum(data))
	return "https://gravatar.com/avatar/" + hash
}

func (user webAuthnUser) WebAuthnCredentials() []webauthn.Credential {
	return user.Credentials()
}

func WebAuthn(user User) webauthn.User {
	return webAuthnUser{&user}
}

func BytesToID(buf []byte) (UserID, error) {
	id, n := binary.Uvarint(buf)
	if n <= 0 {
		return 0, errReadUserID
	}

	return UserID(id), nil
}
