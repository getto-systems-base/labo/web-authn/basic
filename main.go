package main

import (
	"log"
	"net/http"
	"os"

	"github.com/duo-labs/webauthn/webauthn"

	"github.com/gorilla/mux"

	"github.com/getto-systems/webauthn-example/handler"

	"github.com/getto-systems/webauthn-example/infra/datastore/memory"
	"github.com/getto-systems/webauthn-example/infra/random/rand"
	"github.com/getto-systems/webauthn-example/infra/session/cookie"
)

type serverConfig struct {
	RPDisplayName string
	RPID          string
	RPOrigin      string

	TlsCert string
	TlsKey  string
}

func main() {
	config := getServerConfig()

	auth, err := webauthn.New(&webauthn.Config{
		RPDisplayName: config.RPDisplayName,
		RPID:          config.RPID,
		RPOrigin:      config.RPOrigin,
	})
	if err != nil {
		log.Fatal("failed to create WebAuthn from config:", err)
	}

	db := memory.NewDB(rand.NewGenerator())

	store, err := cookie.NewCookieStore()
	if err != nil {
		log.Fatal("failed to create session store:", err)
	}

	router := mux.NewRouter()

	registration := handler.NewRegistrationHandler(*auth, store, db)
	router.HandleFunc("/registration/begin", registration.Begin).Methods("POST")
	router.HandleFunc("/registration/finish", registration.Finish).Methods("POST")

	login := handler.NewLoginHandler(*auth, store, db)
	router.HandleFunc("/login/begin", login.Begin).Methods("POST")
	router.HandleFunc("/login/finish", login.Finish).Methods("POST")

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./public")))

	log.Fatal(http.ListenAndServeTLS(":8080", config.TlsCert, config.TlsKey, router))
}

func getServerConfig() serverConfig {
	return serverConfig{
		RPDisplayName: os.Getenv("RP_DISPLAY_NAME"),
		RPID:          os.Getenv("RP_ID"),
		RPOrigin:      os.Getenv("RP_ORIGIN"),

		TlsCert: os.Getenv("TLS_CERT"),
		TlsKey:  os.Getenv("TLS_KEY"),
	}
}
