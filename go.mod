module github.com/getto-systems/webauthn-example

go 1.13

require (
	github.com/duo-labs/webauthn v0.0.0-20200106204955-07abdb9841e9
	github.com/duo-labs/webauthn.io v0.0.0-20191119211219-328048ff6814
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/sessions v1.2.0
)
