package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

var errBodyNotSend = errors.New("body is not send")

func badRequest(w http.ResponseWriter, err error) {
	log.Println(err)
	jsonResponse(w, err.Error(), http.StatusBadRequest)
}
func forbidden(w http.ResponseWriter, err error) {
	log.Println(err)
	jsonResponse(w, err.Error(), http.StatusForbidden)
}
func internalServerError(w http.ResponseWriter, err error) {
	log.Println(err)
	jsonResponse(w, err.Error(), http.StatusInternalServerError)
}
func ok(w http.ResponseWriter, data interface{}) {
	jsonResponse(w, data, http.StatusOK)
}
func jsonResponse(w http.ResponseWriter, data interface{}, c int) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, "Error creating JSON response", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(c)

	fmt.Fprintf(w, "%s", jsonData)
}
