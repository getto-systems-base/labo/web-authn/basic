package handler

import (
	"encoding/json"
	"net/http"

	"github.com/duo-labs/webauthn/webauthn"

	"github.com/gorilla/sessions"

	"github.com/getto-systems/webauthn-example/models"
	"github.com/getto-systems/webauthn-example/registration"
)

type RegistrationHandler struct {
	webauthn     webauthn.WebAuthn
	sessionStore sessionStore
	datastore    registration.DataStore
}

func NewRegistrationHandler(auth webauthn.WebAuthn, store sessions.Store, datastore registration.DataStore) RegistrationHandler {
	sessionStore := sessionStore{
		key:   "registration",
		store: store,
	}
	return RegistrationHandler{auth, sessionStore, datastore}
}

func (handler RegistrationHandler) Begin(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		badRequest(w, errBodyNotSend)
		return
	}

	var input registration.Input
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		badRequest(w, err)
		return
	}

	err = registration.Validate(input)
	if err != nil {
		badRequest(w, err)
		return
	}

	err = handler.datastore.CheckDuplication(input)
	if err != nil {
		badRequest(w, err)
		return
	}

	userID, err := handler.datastore.GenerateUserID()
	if err != nil {
		internalServerError(w, err)
		return
	}

	var credentials []webauthn.Credential
	user := models.NewUser(userID, input.Email, credentials)

	err = handler.datastore.RegisterUser(*user)
	if err != nil {
		internalServerError(w, err)
		return
	}

	credentialCreationOptions, sessionData, err := handler.webauthn.BeginRegistration(models.WebAuthn(*user))
	if err != nil {
		internalServerError(w, err)
		return
	}

	err = handler.sessionStore.saveSession(sessionData, w, r)
	if err != nil {
		internalServerError(w, err)
		return
	}

	ok(w, credentialCreationOptions)
}

func (handler RegistrationHandler) Finish(w http.ResponseWriter, r *http.Request) {
	sessionData, err := handler.sessionStore.restoreSession(r)
	if err != nil {
		badRequest(w, err)
		return
	}

	userID, err := models.BytesToID(sessionData.UserID)
	if err != nil {
		badRequest(w, err)
		return
	}

	user, err := handler.datastore.LookupUser(userID)
	if err != nil {
		badRequest(w, err)
		return
	}

	credential, err := handler.webauthn.FinishRegistration(models.WebAuthn(*user), *sessionData, r)
	if err != nil {
		internalServerError(w, err)
		return
	}

	err = handler.datastore.RegisterUserCredential(*user, *credential)
	if err != nil {
		internalServerError(w, err)
		return
	}

	ok(w, "Registration Success")
}
