package handler

import (
	"encoding/json"
	"net/http"

	"github.com/duo-labs/webauthn/webauthn"

	"github.com/gorilla/sessions"

	"github.com/getto-systems/webauthn-example/login"
	"github.com/getto-systems/webauthn-example/models"
)

type LoginHandler struct {
	webauthn     webauthn.WebAuthn
	sessionStore sessionStore
	datastore    login.DataStore
}

func NewLoginHandler(auth webauthn.WebAuthn, store sessions.Store, datastore login.DataStore) LoginHandler {
	sessionStore := sessionStore{
		key:   "login",
		store: store,
	}
	return LoginHandler{auth, sessionStore, datastore}
}

func (handler LoginHandler) Begin(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		badRequest(w, errBodyNotSend)
		return
	}

	var input login.Input
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		badRequest(w, err)
		return
	}

	user, err := handler.datastore.LookupUserByInput(input)
	if err != nil {
		badRequest(w, err)
		return
	}

	credentialAssertion, sessionData, err := handler.webauthn.BeginLogin(models.WebAuthn(*user))
	if err != nil {
		internalServerError(w, err)
		return
	}

	err = handler.sessionStore.saveSession(sessionData, w, r)
	if err != nil {
		internalServerError(w, err)
		return
	}

	ok(w, credentialAssertion)
}

func (handler LoginHandler) Finish(w http.ResponseWriter, r *http.Request) {
	sessionData, err := handler.sessionStore.restoreSession(r)
	if err != nil {
		badRequest(w, err)
		return
	}

	userID, err := models.BytesToID(sessionData.UserID)
	if err != nil {
		badRequest(w, err)
		return
	}

	user, err := handler.datastore.LookupUser(userID)
	if err != nil {
		badRequest(w, err)
		return
	}

	credential, err := handler.webauthn.FinishLogin(models.WebAuthn(*user), *sessionData, r)
	if err != nil {
		internalServerError(w, err)
		return
	}

	err = login.Validate(*credential)
	if err != nil {
		forbidden(w, err)
		return
	}

	ok(w, "Login Success")
}
