package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/sessions"

	"github.com/duo-labs/webauthn/webauthn"
)

const sessionKey = "webauthn-session"

type sessionStore struct {
	key   string
	store sessions.Store
}

func (sessionStore sessionStore) saveSession(data *webauthn.SessionData, w http.ResponseWriter, r *http.Request) error {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	session, err := sessionStore.store.Get(r, sessionKey)
	if err != nil {
		log.Printf("new session created : %s", err)
	}

	session.Values[sessionStore.key] = jsonData
	return session.Save(r, w)
}

func (sessionStore sessionStore) restoreSession(r *http.Request) (*webauthn.SessionData, error) {
	session, err := sessionStore.store.Get(r, sessionKey)
	if err != nil {
		return nil, err
	}

	jsonData, ok := session.Values[sessionStore.key].([]byte)
	if !ok {
		return nil, fmt.Errorf("no data in session: %s", sessionStore.key)
	}

	var data webauthn.SessionData
	err = json.Unmarshal(jsonData, &data)
	if err != nil {
		return nil, err
	}

	delete(session.Values, sessionStore.key)

	return &data, nil
}
