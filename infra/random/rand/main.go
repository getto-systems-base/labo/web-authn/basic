package rand

import (
	"crypto/rand"
	"encoding/binary"
)

type Generator struct{}

func NewGenerator() Generator {
	return Generator{}
}

func (Generator) GenerateRandomUint64() (uint64, error) {
	buf := make([]byte, 8)
	_, err := rand.Read(buf)
	if err != nil {
		return 0, err
	}

	return binary.LittleEndian.Uint64(buf), nil
}
