package memory

import (
	"errors"

	"github.com/duo-labs/webauthn/webauthn"

	"github.com/getto-systems/webauthn-example/login"
	"github.com/getto-systems/webauthn-example/models"
	"github.com/getto-systems/webauthn-example/registration"
)

var errUserIDAlreadyExists = errors.New("user id is already exists")
var errEmailAlreadyExists = errors.New("email is already exists")
var errUserIDNotFound = errors.New("user id is not found")
var errEmailNotFound = errors.New("email is not found")

type db struct {
	generator   RandomGenerator
	users       map[models.UserID]userAttr
	emails      map[string]models.UserID
	credentials map[models.UserID]userCredentials
}

type userAttr struct {
	email string
}
type userCredentials struct {
	credentials []webauthn.Credential
}

type RandomGenerator interface {
	GenerateRandomUint64() (uint64, error)
}

func NewDB(generator RandomGenerator) *db {
	return &db{
		generator:   generator,
		users:       make(map[models.UserID]userAttr),
		emails:      make(map[string]models.UserID),
		credentials: make(map[models.UserID]userCredentials),
	}
}

func (db *db) GenerateUserID() (models.UserID, error) {
	id, err := db.generator.GenerateRandomUint64()
	if err != nil {
		return 0, err
	}

	return models.UserID(id), nil
}

func (db *db) CheckDuplication(input registration.Input) error {
	_, ok := db.emails[input.Email]
	if ok {
		return errEmailAlreadyExists
	}

	return nil
}

func (db *db) RegisterUser(user models.User) error {
	userID := user.UserID()
	email := user.Email()

	_, ok := db.users[userID]
	if ok {
		return errUserIDAlreadyExists
	}

	_, ok = db.emails[email]
	if ok {
		return errEmailAlreadyExists
	}

	db.users[userID] = userAttr{
		email: user.Email(),
	}
	db.emails[email] = userID
	db.credentials[userID] = userCredentials{
		credentials: user.Credentials(),
	}

	return nil
}

func (db *db) LookupUser(userID models.UserID) (*models.User, error) {
	return db.restoreUser(userID)
}

func (db *db) LookupUserByInput(input login.Input) (*models.User, error) {
	userID, ok := db.emails[input.Email]
	if !ok {
		return nil, errEmailNotFound
	}

	return db.restoreUser(userID)
}

func (db *db) restoreUser(userID models.UserID) (*models.User, error) {
	user, ok := db.users[userID]
	if !ok {
		return nil, errUserIDNotFound
	}

	credentials, ok := db.credentials[userID]
	if !ok {
		return nil, errUserIDNotFound
	}

	return models.NewUser(userID, user.email, credentials.credentials), nil
}

func (db *db) RegisterUserCredential(user models.User, credential webauthn.Credential) error {
	userID := user.UserID()

	data, ok := db.credentials[userID]
	if !ok {
		return errUserIDNotFound
	}

	data.credentials = append(data.credentials, credential)

	db.credentials[userID] = data

	return nil
}
