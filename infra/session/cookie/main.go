package cookie

import (
	"crypto/rand"
	"errors"

	"github.com/gorilla/sessions"
)

const CookieStoreAuthenticationKeyLength = 64
const CookieStoreEncryptionKeyLength = 32

var ErrGenerateSecureKeyInsufficientBytesRead = errors.New("generate secure key failed: insufficient bytes read")

func NewCookieStore() (*sessions.CookieStore, error) {
	keyPairs := make([][]byte, 2)

	authenticationKey, err := generateSecureKey(CookieStoreAuthenticationKeyLength)
	if err != nil {
		return nil, err
	}

	encryptionKey, err := generateSecureKey(CookieStoreEncryptionKeyLength)
	if err != nil {
		return nil, err
	}

	keyPairs = append(keyPairs, authenticationKey)
	keyPairs = append(keyPairs, encryptionKey)

	store := sessions.NewCookieStore(keyPairs...)

	return store, nil
}
func generateSecureKey(n int) ([]byte, error) {
	buf := make([]byte, n)

	read, err := rand.Read(buf)
	if err != nil {
		return buf, err
	}

	if read != n {
		return buf, ErrGenerateSecureKeyInsufficientBytesRead
	}

	return buf, nil
}
